import logo from './logo.svg';
import './App.css';
import Header from './Component/Header/Header';
import MainDashboard from './Component/dashboard/MainDashboard';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css"
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Footer from './Component/Footer/Footer';
import AllProducts from './Component/products/AllProducts';

import ProductDetails from './Component/productDetails/ProductDetails';

function App() {
  return (
    <div>
      <div>
        <Header />
      </div>
      <BrowserRouter>
        <Routes>
          <Route exact path='/' element={<MainDashboard />} />
          <Route exact path='/details' element={<ProductDetails />} />

        </Routes>
      </BrowserRouter>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default App;
