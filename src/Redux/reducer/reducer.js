const initialState = {
    product_details_Id: ""

}

const stateReducer = (state = initialState, action) => {
    switch (action.type) {
        case "product_details_Id":
            return {
                ...state,
                product_details_Id: action.payload

            }
        default: return state;
    }
}

export default stateReducer;