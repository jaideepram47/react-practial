import React, { useEffect, useState } from "react";
import { Button, Container, Row } from "react-bootstrap";
import { useDispatch,useSelector } from "react-redux";
import "./ProductDetails.css";
import {Link, useNavigate} from "react-router-dom"
import {
  faCarrot,
  faPepperHot,
  faAppleWhole,
  faEllipsisVertical,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import fruits from "../../fruits.json"
import vegetables from "../../vegetables.json"
import { product_details_Id } from "../../Redux/action/Action";


const ProductDetails = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate();
  const product_State = useSelector((state) => state?.product_details_Id);
  const [productData, setProductData] = useState(product_State);
  const [value, setValue] = useState("all");
  const [number, setNumber] = useState(0);
  const [totalBill, setTotalBill] = useState(0);
  const [productType, setProductType] = useState(productData?.type == "fruits" ? fruits : vegetables);

  useEffect(()=>{
    setTotalBill(0)
    setNumber(0)
  },[productData])

let data = productType?.filter((item)=>{
  return item?.category == productData?.category
})

const handleBilling =()=>{
  let total = number * productData?.price.split(" ")[1]
  setTotalBill(total)
}
const handleRoute = (e,item) =>{  
  setProductData(item)
  dispatch(product_details_Id(item))
  // navigate("../details", { replace: true });
}
  return (
    <React.Fragment>
      <Container>
        <div >
          <div className="bredCrum">
          <Link className="bread" to={"/"}>Home</Link> | <a className="text-primary" style={{ textDecoration: "none" }}>Contact Us</a>
          </div>
        <div className="d-flex ">
          <div className="imageContainer">
            <img className="imageSize" src={productData?.image} alt="" />
          </div>
          <div>
            <div>
              <h2>{productData?.nameDetails}</h2>
              <div className="d-flex">
                <p>
                  Sold by:<span>Haries Market</span>
                </p>
                <p className="status">
                  Status:<span>In Stock</span>
                </p>
              </div>
              <div>
                <h2>
                  {productData?.price+"/item "}
                  <span>{`(${productData?.perKg})`}</span>
                </h2>
              </div>
              <div className="d-flex">
                <div className="d-flex">
                  <Button
                    className="quantityMinus"
                    disabled={number == 0 ? true : false}
                    onClick={() => setNumber(number - 1)}
                  >
                    -
                  </Button>
                  <input className="quantityInput" value={number} readOnly />
                  <Button
                    className="quantityPlus"
                    onClick={() => setNumber(number + 1)}
                  >
                    +
                  </Button>
                </div>
                <div>
                  <Button className="addTocart" onClick={handleBilling}>Add To Cart</Button>
                </div>
              </div>
              <div className="category">
                <p>Category: {productData?.category}</p>
              </div>
              <div>
                <h2>{totalBill > 0 &&  `Total Bill: $${totalBill}`}</h2>
              </div>
            </div>
          </div>
        </div>
        <div>
        <div className="mainProductContainer">
            <div className="filterComponent">
              <div className="allComponent">
                {/* <span>
                  <FontAwesomeIcon icon={faCarrot} className="hello" />
                </span> */}
                <Button variant="light" className={value == "all" ? "filterButtonActive" : "filterButton"} onClick={()=>setValue("all")}>Information</Button>
              </div>
            </div>
            <div className="filterComponent">
              <div className="allComponent">
                {/* <span>
                  <FontAwesomeIcon icon={faAppleWhole} className="hello" />
                </span> */}
                <Button variant="light" className={value == "fruits" ? "filterButtonActive" : "filterButton"} onClick={()=>setValue("fruits")}>Ingredients</Button>
              </div>
            </div>
            <div className="filterComponent">
              <div className="allComponent">
                {/* <span>
                  <FontAwesomeIcon icon={faPepperHot} className="hello" />
                </span> */}
                <Button variant="light" className={value == "vegetables" ? "filterButtonActive" : "filterButton"} onClick={()=>setValue("vegetables")}>Directions</Button>
              </div>
            </div>
          </div>
        </div>
        <div>
          {value == "all" ? <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio necessitatibus error corporis fugit rerum sed totam sit eaque reiciendis autem quas id consectetur laborum facere laudantium ex, vitae placeat molestias animi nisi similique quae enim amet sequi. Impedit, laudantium. Commodi.</p> : value == "fruits" ? <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eveniet velit beatae libero ipsa! Quis ex facilis libero odio. Tempora voluptates provident nisi. Ab maiores quam vitae dignissimos perferendis. Dicta ad quam ullam, adipisci consectetur vero explicabo facere deleniti voluptatem beatae necessitatibus itaque voluptatibus. Pariatur commodi saepe voluptates repudiandae asperiores suscipit.</p> :  <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolor doloribus quod exercitationem doloremque saepe, distinctio corrupti. Vero veniam dicta iure, fugiat a non ratione unde pariatur ad sed, minima eos fugit illum praesentium beatae optio omnis. Necessitatibus, perferendis? Reiciendis odio ad mollitia. Deleniti cupiditate id animi at quidem sunt consectetur voluptatibus. Tempore at adipisci sunt! Optio ipsum iure eveniet mollitia.</p>}
          
         
        </div>
        <div>
        <div>
          <h1>Related Products</h1>
        </div>
        <div>
        <div className="hero">
        {data?.map((item, index) => {
                  return (
                    <React.Fragment key={index}>
                      <div className="card-container" onClick={(e)=>handleRoute(e,item)}>
                        <div className="image-container">
                          <span className="likeLogo1">
                            <FontAwesomeIcon icon={faEllipsisVertical} />
                          </span>
                          <span className="likeLogo2">
                            <FontAwesomeIcon icon={faAppleWhole} />
                          </span>
                          <img src={item?.image} alt="" />
                        </div>
                        <div className="card-title">
                          <h3>{item?.title}</h3>
                        </div>
                        <div className="card-body">
                          <p className="fw-bold">{item?.price} <span className="fw-light">{item?.perKg}</span>
                          </p>
                        </div>
                        <div className="btn">
                          <button className="addTocart">
                            <a>Add to cart</a>
                          </button>
                        </div>
                      </div>
                    </React.Fragment>
                  );
                })}
        </div>
        </div>
        </div>
        </div>
      </Container>
    </React.Fragment>
  );
};

export default ProductDetails;
