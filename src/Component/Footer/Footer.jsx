import React from 'react'
import { Container } from 'react-bootstrap'
import "./Footer.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faFaceSmile} from "@fortawesome/free-solid-svg-icons";

const Footer = () => {
  return (
    <React.Fragment>
            <div className='mainFooter'>
                <h1>.</h1>
        <Container className='mainFooterContainer'>
                <div className='d-flex footerDivision'>
                    <div>
                        <h6>POPULAR SUBRUBS</h6>
                        <p>Vegetable</p>
                        <p>Fruits</p>
                        <p>Home Delivery</p>
                        <p>Ahmedabad</p>
                    </div>
                    <div>
                        <h6>PAGES</h6>
                        <p>About US</p>
                        <p>Terms & Conditions</p>
                        <p>Contact Us</p>
                        <p>24/7</p>
                    </div>
                    <div>
                        <h6>KEEPS IN TOUCH</h6>
                        <p>Contact Us</p>
                        <p>Help Center</p>
                        <div>
                            <span><FontAwesomeIcon icon={faFaceSmile}/></span>
                            <span className='emoji'><FontAwesomeIcon icon={faFaceSmile}/></span>
                        </div>
                        
                    </div>
                </div>
        </Container>
            </div>
    </React.Fragment>
  )
}

export default Footer