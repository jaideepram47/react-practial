import React, { useState } from "react";
import { Button, Container, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCarrot,
  faPepperHot,
  faAppleWhole,
  faEllipsisVertical,
} from "@fortawesome/free-solid-svg-icons";
import "../products/AllProduct.css";
import apple from "../../Assets/Img/apple.jpg";
import vegetables from "../../Assets/Img/vegetables.jpg";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { product_details_Id } from "../../Redux/action/Action";
import fruitsData from "../../fruits.json"
import Vegetables from "../../vegetables.json"

const AllProducts = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch()
  
 

  const [value, setValue] = useState("all"); 
  const [number, setNumber] = useState(1); 
  const postPerPage = 5;

  const lastPost = number * postPerPage;
  const firstPost = lastPost - postPerPage;
  const currentPost1 = fruitsData?.slice(firstPost, lastPost);
  const currentPost2 = Vegetables?.slice(firstPost, lastPost);
  
const handleRoute = (e,item) =>{  
  dispatch(product_details_Id(item))
  navigate("../details", { replace: true });
}

  return (
    <React.Fragment>
      <Row>
        <Container>
          <div className="mainProductContainer">
            <div className="filterComponent">
              <div className="allComponent">
                <span>
                  <FontAwesomeIcon icon={faCarrot} className="hello" />
                </span>
                <Button variant="light" className={value == "all" ? "filterButtonActive" : "filterButton"} onClick={()=>setValue("all")}>All</Button>
              </div>
            </div>
            <div className="filterComponent">
              <div className="allComponent">
                <span>
                  <FontAwesomeIcon icon={faAppleWhole} className="hello" />
                </span>
                <Button variant="light" className={value == "fruits" ? "filterButtonActive" : "filterButton"} onClick={()=>setValue("fruits")}>Fruits</Button>
              </div>
            </div>
            <div className="filterComponent">
              <div className="allComponent">
                <span>
                  <FontAwesomeIcon icon={faPepperHot} className="hello" />
                </span>
                <Button variant="light" className={value == "vegetables" ? "filterButtonActive" : "filterButton"} onClick={()=>setValue("vegetables")}>Vegitable</Button>
              </div>
            </div>
          </div>
          {value == "fruits" ||value == "all" ? <div>
            <div className="container filterProducts">
              <h1 className="headingUnderLine">Fruits</h1>
              <div className="hero">
                {currentPost1?.map((item, index) => {
                  return (
                    <React.Fragment key={index}>
                      <div className="card-container" onClick={(e)=>handleRoute(e,item)}>
                        <div className="image-container">
                          <span className="likeLogo1">
                            <FontAwesomeIcon icon={faEllipsisVertical} />
                          </span>
                          <span className="likeLogo2">
                            <FontAwesomeIcon icon={faAppleWhole} />
                          </span>
                          <img src={item?.image} alt="" />
                        </div>
                        <div className="card-title">
                          <h3>{item?.title}</h3>
                        </div>
                        <div className="card-body">
                          <p className="fw-bold">{item?.price} <span className="fw-light">{item?.perKg}</span>
                          </p>
                        </div>
                        <div className="btn">
                          <button className="addTocart">
                            <a>Add to cart</a>
                          </button>
                        </div>
                      </div>
                    </React.Fragment>
                  );
                })}
              </div>
              <div className="">
                <Button   
                  disabled={firstPost == 0 ? true : false}
                  className="mx-3  border-0"
                  onClick={() => setNumber(number - 1)}
                >
                  Left
                </Button>
                <Button
                  disabled={currentPost1?.length <= 4 ? true : false}
                  className="mx-3  border-0"
                  onClick={() => setNumber(number + 1)}
                >
                  Right
                </Button>
              </div>
            </div>
          </div> : null}
          {value == "vegetables"||value == "all" ? <div className="container filterProducts">
            <h1 className="headingUnderLine">Vegetables</h1>
            <div className="hero">
              {currentPost2?.map((item, index) => {
                return (
                  <React.Fragment key={index}>
                    <div className="card-container" onClick={(e)=>handleRoute(e,item)}>
                      <div className="image-container">
                        <span className="likeLogo1">
                          <FontAwesomeIcon icon={faEllipsisVertical} />
                        </span>
                        <span className="likeLogo2">
                          <FontAwesomeIcon icon={faAppleWhole} />
                        </span>
                        <img src={item?.image} alt="" />
                      </div>
                      <div className="card-title">
                        <h3>{item?.title}</h3>
                      </div>
                      <div className="card-body">
                        <p className="fw-bold">
                        {item?.price}<span className="fw-light">{item?.perKg}</span>
                        </p>
                      </div>
                      <div className="btn">
                        <button className="addTocart">
                          <a>Add to cart</a>
                        </button>
                      </div>
                    </div>
                  </React.Fragment>
                );
              })}
            </div>
            <div className="">
                <Button   
                  disabled={firstPost == 0 ? true : false}
                  className="mx-3  border-0"
                  onClick={() => setNumber(number - 1)}
                >
                  Left
                </Button>
                <Button
                  disabled={currentPost1?.length <= 4 ? true : false}
                  className="mx-3  border-0"
                  onClick={() => setNumber(number + 1)}
                >
                  Right
                </Button>
              </div>
          </div> : null}
        </Container>
      </Row>
    </React.Fragment>
  );
};

export default AllProducts;
