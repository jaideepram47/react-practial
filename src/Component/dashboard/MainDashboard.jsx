import React, { useState } from "react";
import "./Maindashboard.css";
import { Link } from "react-router-dom";
import { Navbar, Container, Offcanvas, Nav, Modal,Form,Button,InputGroup } from "react-bootstrap";
import logo from "../../Assets/Img/logo1.jfif";
import Rating from "@mui/material/Rating";
import AllProducts from "../products/AllProducts";



const MainDashboard = () => {
  const [value, setValue] = useState(2);
  return (
    <div>
      <React.Fragment>
        <div className="mainContainer" style={{ opacity: "o.4" }}>
          <Container>
            <div>
              <h5 className="f-19 m-0 d-flex bredcrum">
                <a> Home </a> | <a>Shops</a> | <a>Haris Farm Market</a>
              </h5>
            </div>
            <div className="dashboardLogoContainer">
              <div>
                <img src={logo} className="imgLogo" />
                <h1>Harries Farm Market</h1>
              </div>
              <div className="d-flex rating">
                <Rating
                  name="simple-controlled"
                  value={value}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                  }}
                />
                <p className="norating">No rating found yet</p>
              </div>
              <div className="moreInfoBtn">
                <button className="commonBtn">More Info</button>
                <button className="commonBtn">Give a review</button>
              </div>
              <div className="w-100">
               <div className="mainInput">
               <InputGroup className="mb-3 searchBar">
                  <Form.Control className="dashboardInput"
                    placeholder="Search harries farm product"
                    aria-label="Recipient's username"
                    aria-describedby="basic-addon2"
                  />
                  <Button variant="outline-secondary" id="button-addon2" className="inputBtn">
                   Search
                  </Button>
                </InputGroup>
               </div>
              </div>
            </div>
          </Container>
        </div>
        <div>
          <AllProducts/>
        </div>
      </React.Fragment>
    </div>
  );
};

export default MainDashboard;
