import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCoffee,
  faBars,
  faBagShopping,
  faPaperPlane,
  faClock,
  faShop,
  faUser,
  faHeart
} from "@fortawesome/free-solid-svg-icons";
import { Navbar, Container, Offcanvas, Nav, Modal } from "react-bootstrap";
import icon from "../../Assets/Img/icon-cart.svg";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";
import "./Header.css";
import { faBell } from "@fortawesome/free-regular-svg-icons";

const Header = () => {
  return (
    <React.Fragment>
      <Navbar bg="light" expand="md" className="py-4">
        <Container>
          <div>
            <FontAwesomeIcon icon={faBars} />
          </div>
          <div className="rightSideNavigator">
            <div className="input-icons">
              <span>
                <FontAwesomeIcon icon={faPaperPlane} />
              </span>
              <input
                className="input-field"
                placeholder="Delivery Address/Post Code"
              />
            </div>
            <div className="deliveryDropDown">
              <span>
                <FontAwesomeIcon icon={faClock} />
              </span>
              <Dropdown as={ButtonGroup}>
                <Button variant="none">Delivery ASAP</Button>

                <Dropdown.Toggle
                  split
                  variant="none"
                  id="dropdown-split-basic"
                />

                <Dropdown.Menu>
                  <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">
                    Another action
                  </Dropdown.Item>
                  
                </Dropdown.Menu>
              </Dropdown>
            </div>
            <div>
              <button className="shopBtn">
                <FontAwesomeIcon icon={faShop} className="iconColor" /> Shop
              </button>
            </div>
            <div className="deliveryDropDown">
            <span>
                <FontAwesomeIcon icon={faUser} />
              </span>
              <Dropdown as={ButtonGroup}>
                <Button variant="none">Sign In</Button>

                <Dropdown.Toggle
                  split
                  variant="none"
                  id="dropdown-split-basic"
                />

                <Dropdown.Menu>
                  <Dropdown.Item href="#/action-1">Logout</Dropdown.Item>
                  <Dropdown.Item href="#/action-2">
                    View Profile
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
            <div>
              <button className="notificationBtn">
                <FontAwesomeIcon icon={faHeart} className="notificationIcon"/>
              </button>
              </div>
            <div>
              <button className="notificationBtn">
                <FontAwesomeIcon icon={faBagShopping} className="notificationIcon"/>
              </button>
            </div>
          </div>
        </Container>
      </Navbar>
    </React.Fragment>
  );
};

export default Header;
